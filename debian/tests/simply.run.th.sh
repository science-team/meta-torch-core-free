#!/bin/sh
set -e

# we expect user to be able to use 'th' (Torch REPL) with this metapackage.
th -e "print('torch is at your service.')"

exit 0
