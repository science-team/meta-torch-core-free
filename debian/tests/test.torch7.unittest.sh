#!/bin/sh
set -e

# let's run torch7's unittest
th -e "require 'torch'; torch.test();"

exit 0
